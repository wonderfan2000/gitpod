#!/usr/bin/env bash

function ssh(){
    cp ssh/* ~/.ssh/
}

function config(){
  echo $1
  cd $1
  git config --local user.name threewebcode
  git config --local user.email magestore@outlook.com
}

$@