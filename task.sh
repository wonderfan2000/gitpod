#!/usr/bin/env bash

find erhai-chain/proto -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zeus|erhai|g'

cp -R x/* module

find erhai-chain -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zeus|erhai|g'
find erhai-chain -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|Zeus|Erhai|g'

find erhai-app -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zeusd|hcd|g'
find erhai-app -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zeus|erhai|g'
find erhai-app -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|Zeus|Erhai|g'

find mod-ibc -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|Zeus|Erhai|g'

mv zhigui-projects loong-projects