#!/usr/bin/env bash
set -x

function pullcode() {
    cd mychain
    local token=uahS8T8kzid8Sr6jUxyF
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-chain.git 
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-app.git
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-jvm.git 
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-perm.git 
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-tbft.git
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-archive.git
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-db.git
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-ibc.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-gov.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-wasm.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-evm.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-feemarket.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/mod-upgrade.git
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-jvm.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-db-sync.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-mq.git
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-dashboard.git
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-sdk-go.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/go-sdk-proxy.git
    git clone -b master https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-adapter.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-wallet.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/zeus-spv.git
    git clone -b main https://oauth2:$token@gitlab.ziggurat.cn/Zeus-Network/gm-card.git
}

function debug() {
    echo "hello shell $PWD"
}

function pushtag() {
    echo "start tag"
    tagname="v0.2.0"
    projects[0]="zeus-app"
    projects[1]="zeus-chain"
    projects[2]="zeus-db"
    projects[3]="zeus-jvm"
    projects[4]="mod-evm"
    projects[5]="mod-wasm"
    projects[6]="mod-jvm"
    projects[7]="mod-tbft"
    projects[8]="mod-ibc"
    projects[9]="mod-perm"
    projects[10]="mod-upgrade"
    projects[11]="mod-feemarket"
    projects[12]="mod-archive"
    projects[13]="mod-gov"
    projects[14]="zeus-sdk-go"
    projects[15]="go-sdk-proxy"
    wd=$PWD
    for project in "${projects[@]}"; do
        echo $project
        cd mychain/$project
        git config --local user.email "fanjiahe@zhigui.com"
        git config --local user.name "fanjiahe"
        git tag -a $tagname -m "release $tagname"
        git push origin $tagname
        cd $wd
        sleep 1s
    done
}

function pushbranch() {
    echo "start push"
    branchname="bc-v0.2.0"
    projects[0]="zeus-app"
    projects[1]="zeus-chain"
    projects[2]="zeus-db"
    projects[3]="zeus-jvm"
    projects[4]="mod-evm"
    projects[5]="mod-wasm"
    projects[6]="mod-jvm"
    projects[7]="mod-tbft"
    projects[8]="mod-ibc"
    projects[9]="mod-perm"
    projects[10]="mod-upgrade"
    projects[11]="mod-feemarket"
    projects[12]="mod-archive"
    projects[13]="mod-gov"
    projects[14]="zeus-sdk-go"
    projects[15]="go-sdk-proxy"
    wd=$PWD
    for project in "${projects[@]}"; do
        echo $project
        cd mychain/$project
        git config --local user.email "fanjiahe@zhigui.com"
        git config --local user.name "fanjiahe"
        git branch $branchname
        git push -u origin $branchname
        cd $wd
        sleep 1s
    done
}

$@