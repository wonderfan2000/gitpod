#!/usr/bin/env bash
set -x

git clone ssh://git@gitlab.ziggurat.cn:10000/loong-projects/main.git

cd main 

git submodule init

git submodule update

git submodule foreach git log --oneline --graph -n 2

