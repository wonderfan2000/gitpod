#!/usr/bin/env bash

for item in erhai-chain erhai-app erhai-db erhai-jvm erhai-sdk-go go-sdk-proxy mod-evm mod-wasm mod-jvm mod-tbft mod-ibc mod-perm mod-upgrade mod-feemarket mod-archive mod-gov; do
    echo loong-projects/$item
    cp -R loong-projects/$item/* mycode/$item
done