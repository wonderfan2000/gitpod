#!/usr/bin/env bash

sui client
sui client addresses

curl --location --request POST 'https://faucet.devnet.sui.io/gas' \
--header 'Content-Type: application/json' \
--data-raw '{
    "FixedAmountRequest": {
        "recipient": "0x4900c8dbe68e2984205a5588e799ac3f38094b69642c15547691a57451e17d26"
    }
}'

sui client gas 0x4900c8dbe68e2984205a5588e799ac3f38094b69642c15547691a57451e17d26

sui client publish --gas-budget 30000 box

SUI_RPC_HOST="https://fullnode.testnet.sui.io:443"
curl --location --request POST $SUI_RPC_HOST \
--header 'Content-Type: application/json' \
--data-raw '{
"jsonrpc": "2.0",
"id": 1,
"method": "rpc.discover",
"params":[]
}' | jq .result.info.version | tr -d \"