#!/usr/bin/env bash

set -x

for mp in mod-*; do
    find "$mp" -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zeusd|hcd|g'
    find "$mp" -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zeus|erhai|g'
    find "$mp" -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|Zeus|Erhai|g'
    cd $mp
    make proto
    cd ..
done