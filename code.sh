#!/bin/bash

# Generate SSH key with comment
ssh-keygen -t rsa -b 4096 -C "threewebcode"

# Backup existing SSH files
mkdir -p ~/ssh_backup
cp ~/.ssh/* ~/ssh_backup
