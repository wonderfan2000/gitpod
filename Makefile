.PHONY: github

github:
	@read -rp "Enter commit message " message && \
	git add --all && \
	git commit -m "$$message" && \
	git push