#!/usr/bin/env bash

make image-prod
make localClusterStart
#  copy contract class into target directory
contractDir=/workspace/gitpod/main/erhai-jvm/erhai-jvm-contract/target/classes/com/loong/erhai/jvm/contract
cp ${contractDir}/BatchStorageContract.class /workspace/gitpod/main/deploy/fake-cluster/clusterConfig/

cd /workspace/gitpod/main/deploy/fake-cluster/clusterConfig/

hcd tx jvm deploy-contract "batch:1.0" BatchStorageContract.class --from admin --home node0 --chain-id erhai -b block -y
hcd tx jvm execute-contract "batch:1.0" "setData" '{"args":["c1000","10"]}' --from admin --home node0 --chain-id erhai -b block -y
hcd query jvm contract "batch:1.0" "getData" '{"args":["b1000"]}' --home node0


while true; do
    hcd query jvm contract "batch:1.0" "getData" '{"args":["a1000"]}' --home node0
done

find . -name jvm_config.yaml -exec sed -i -e "s/500000/10000000000000000000000/g" {} \;

sudo find . -name jvm_config.yaml -exec sed -i -e "s/10000000000000000000000/50000000/g" {} \;

docker compose restart

docker compose logs -f --tail 50 

hcd tx jvm deploy-contract "batch:1.0" BatchStorageContract.class --from alice --home sm2 --chain-id mychain -b block -y

hcd tx jvm execute-contract "batch:1.0" "setData" '{"args":["c1000","1000000"]}' --from alice --home sm2 --chain-id mychain -b block -y